﻿using Abp.AutoMapper;
using Abp.Runtime.Validation;
using JTTMS.App.Drivers.Dto;
using JTTMS.THEntity.$TableNames;
using JTTMS.Utilty;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using static JTTMS.JTTMSEnum.JTTMSEnum;

namespace JTTMS.App.$TableNames.Dto
{
    /// <summary>
    /// $CNName创建模型
    /// </summary>
    [AutoMapTo(typeof($TableName))]
    public class $TableNameCreateInput : ICustomValidate, IShouldNormalize
    {
        
        /// <summary>
        /// 自定义验证规则
        /// </summary>
        /// <param name="context"></param>
        public void AddValidationErrors(CustomValidationContext context)
        {
            //if (!Title.Contains("测试"))
            //{
            //    context.Results.Add(new ValidationResult("标题必须包含。。。"));
            //}
            //context.Results.Add(new ValidationResult("错误信息"));或 throw new UserFriendlyException("错误信息");
        }

        /// <summary>
        /// 数据验证之后的处理
        /// </summary>
        public void Normalize()
        {
        }
    }
}
